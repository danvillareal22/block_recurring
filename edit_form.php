<?php

class block_recurring_edit_form extends block_edit_form {

	protected function specific_definition($mform) {

		global $DB, $COURSE, $CFG;
					
		$mform->addElement('header', 'configureheader', get_string('recurring', 'block_recurring'));

		$gradeitems = $DB->get_records_sql("SELECT GI.*, C.fullname, C.category, C.idnumber as 'courseidnumber'
											FROM {grade_items} GI, {course} C
											WHERE GI.courseid = C.id AND C.id = ?", array($COURSE->id));

		$gradeoptions = array();

		foreach ($gradeitems as $gradeitem) {
 			$name = '';
 			if (!empty($gradeitem->idnumber)) {
 				$name .= '[' .$gradeitem->idnumber . '] ';
 			}
 			if ($gradeitem->itemtype == 'course') {
 				if (!empty($gradeitem->courseidnumber)) {
 					$name .= '[' . $gradeitem->courseidnumber . ']' . $gradeitem->fullname . ' (Course Final Grade)';
 				} else {
 					$name .= $gradeitem->fullname . ' (Course Final Grade)';
 				}
 				
 			} else {
 				if (!empty($gradeitem->itemname)) {
 					$name .= $gradeitem->itemname . ' ';
 				}
 				if ($gradeitem->itemtype == 'manual') {
 					$name .= '(Manual Item)';
 				} else if ($gradeitem->itemmodule == 'scorm') {
 					$name .= '(eLearning)';
 				}
 			}

 			$gradeoptions[$gradeitem->id] = $name;
 		}
	
		$mform->addElement('hidden', 'config_blocktitle', get_string('instance_title', 'block_recurring'));
		$mform->setDefault('config_blocktitle', get_string('recurring', 'block_recurring'));
		$mform->setType('config_blocktitle', PARAM_RAW);

		$mform->addElement('hidden', 'config_activeblock', get_string('instance_activeblock', 'block_recurring'), get_string('activeblock_help', 'block_recurring'));
		$mform->setDefault('config_activeblock', true);
		$mform->setType("config_activeblock", PARAM_BOOL);

		$mform->addElement('select', 'config_recurringgradeid', 'Recurring Grade Item', $gradeoptions);

		$mform->addElement('text', 'config_competencylength', get_string('competencylength', 'block_recurring'));
		$mform->setDefault('config_competencylength', get_string('competencyperiod', 'block_recurring'));
		$mform->setType('config_competencylength', PARAM_INT);  

		$mform->addElement('hidden', 'config_unlockperiod', get_string('unlock', 'block_recurring'));
		$mform->setDefault('config_unlockperiod', get_string('unlockperiod', 'block_recurring'));
		$mform->setType('config_unlockperiod', PARAM_INT);  
	
		$mform->addElement('hidden', 'config_datemodified', date("Y-m-d H:i:s"));
		$mform->setType('config_datemodified', PARAM_RAW);

	}

	public function specialization() {

	    if (isset($this->config)) {
	        if (empty($this->config->title)) {
	            $this->title = get_string('defaulttitle', 'block_recurring');            
	        } else {
	            $this->title = $this->config->title;
	        }
	 
	        if (empty($this->config->text)) {
	            $this->config->text = get_string('defaulttext', 'block_reucrring');
	        }    
	    }
	    
	}

	public function instance_config_save($data) {

 		$data = stripslashes_recursive($data);
  		$this->config = $data;

  		echo '<pre>';
  		print_r($data);
  		die('here');

  		return set_field(	'block_instance', 'configdata',
                    		base64_encode(serialize($data)), 'id', 
                   			$this->instance->id
		);
	}
}