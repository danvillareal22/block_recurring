<?php

$string['pluginname'] = 'Recurring Training';
$string['recurring'] = 'Recurring Training';
$string['recurring:addinstance'] = 'Add a new recurring training block';
$string['instancetitle'] = 'Instance Title';
$string['instance_title'] = 'Block instance title';
$string['instance_activeblock'] = '';
$string['activeblock_help'] = 'Actively run the training as recurring';
$string['headerconfig'] = 'Recurring';
$sring['desconfig'] = 'a';
$string['labelallowrecurring'] = 'labelallowrecurring';
$string['lockout'] = 'Lockout Period';
$string['unlock'] = 'Unlock Period';
$string['unlockperiod'] = '0';
$string['lockoutperiod'] = '0';
$string['lockoutlength'] = 'Lockout Frequency';
$string['competencylength'] = 'Days until Expired';
$string['competencyperiod'] = '365';
$string['recurringcondition'] = 'Recurring Condition';