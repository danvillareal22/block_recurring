<?php

class block_recurring extends block_base {

	public function init() {
		global $CFG, $DB, $COURSE;

		$this->title = get_string('pluginname', 'block_recurring');
	}
	
	public function specialization() {
		if (isset($this->config)) {
			if (empty($this->config->title)) {
				$this->title = get_string('recurring', 'block_recurring');            
			} else {
				$this->title = $this->config->title;
			}

			if (empty($this->config->text)) {
				$this->config->text = get_string('recurring', 'block_recurring');
			}    
		}
	}

	public function instance_allow_multiple() {
		return true;
	}

	public function is_empty() {

		global $COURSE;

		$coursecontext = context_course::instance($COURSE->id);

		if (has_capability('moodle/course:viewhiddensections', $coursecontext)) {
			return true;
		} else {
			return false;
		}

	}

	public function has_config() {
		return false;
	}

	public function instance_allow_config() {
		return true;
	}
	
	public function get_content() {
		global $DB, $COURSE, $USER, $CFG, $PAGE;

		if ($this->content !== null) {
			return $this->content;
		}
		
		$coursecontext = context_course::instance($COURSE->id);

		$this->content = new stdClass;

		if(isset($this->content->text)== null){
			$this->content->text = ""; 
		}

		if(is_numeric($this->config->recurringgradeid)) {

			if (has_capability('moodle/course:viewhiddensections', $coursecontext)) {

				$gradeitem = $DB->get_record_sql("SELECT * FROM {grade_items} WHERE id = ?", array($this->config->recurringgradeid));

				if ($gradeitem) {
					$name = '';
		 			if (!empty($gradeitem->idnumber)) {
		 				$name .= '[' .$gradeitem->idnumber . '] ';
		 			}
		 			if ($gradeitem->itemtype == 'course') {
		 				if (!empty($gradeitem->courseidnumber)) {
		 					$name .= '[' . $gradeitem->courseidnumber . ']' . $gradeitem->fullname . ' (Course Final Grade)';
		 				} else {
		 					$name .= $gradeitem->fullname . ' (Course Final Grade)';
		 				}
		 				
		 			} else {
		 				if (!empty($gradeitem->itemname)) {
		 					$name .= $gradeitem->itemname . ' ';
		 				}
		 				if ($gradeitem->itemtype == 'manual') {
		 					$name .= '(Manual Item)';
		 				} else if ($gradeitem->itemmodule == 'scorm') {
		 					$name .= '(eLearning)';
		 				}
		 			}

		 			$this->config->competencylength = 1;

		 			$days = 'day';
		 			if ($this->config->competencylength > 1) {
		 				$days = 'days';
		 			}

		 			$this->content->text .= "<p><strong>$name</strong> is configured to expire every <strong>{$this->config->competencylength}</strong> $days.</p>";

				}

				$this->content->text  .= "<p><a href='".$CFG->wwwroot."/local/learnbook/report/index.php' style='color: #333' class='btn btn-block btn-default'>Access Reports</a></p>";
			}

		} else {

			$this->content->text = "<p>Recurring Training is not configured for this block.</p>
			<p>Here's how to enable it:</p>
			<ul>
				<li><strong>Turn Editing On</strong>.</li>
				<li>Select the <strong>Gear</strong> icon from the <strong>top right</strong> of this block and select \"<strong>Configure Recurring Training</strong>\" block.</li>
				<li>Select the <strong>Grade Item</strong> which should be marked as Expired after a set period of time.</li>
				<li>Enter enter how many days until a <strong>Grade Item</strong> is marked expired.</li>
				<li>Select <strong>Save Changes</strong>.</li>
			</ul>
			";
		}

		$this->content->text .= "<p><i>Note: Expiry information is processed in the background.</i></p>
			<p><i>It may take up to 24 hours for this information to first appear in Learnbook Reports.</i></p>";

		return $this->content;
	}

	public function applicable_formats() {
		return array(
			'course-view' => true,
		);
	}
	
}
