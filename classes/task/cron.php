<?php

namespace block_recurring\task;

class cron extends \core\task\scheduled_task {   

	public function get_name() {
		return 'Update Expiry Information';
	}

	public function execute() {

		global $DB;

		$recurringblocks = $DB->get_records_sql("SELECT * FROM mdl_block_instances WHERE blockname = 'recurring'");

		foreach ($recurringblocks as $recurringblock) {

			if (!$recurringblockconfig->competencylength) {
				continue;
			}

			$recurringblockconfig = unserialize(base64_decode($recurringblock->configdata));
			
			$competencylengthdays = $recurringblockconfig->competencylength;

			mtrace('Updating Expired Records');

			$expiredTrainingItems = $DB->get_recordset_sql("SELECT GG.*, GI.courseid FROM mdl_grade_grades GG, mdl_grade_items GI WHERE GG.itemid = GI.id AND GG.itemid = $recurringblockconfig->recurringgradeid AND GG.timemodified <= (SELECT UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL $competencylengthdays DAY)))");

			foreach ($expiredTrainingItems as $expiredTrainingItem) {

				$expiredtime = $expiredTrainingItem->timemodified + ($competencylengthdays * 86400);

				$trainingstatus = new \stdClass();
				$trainingstatus->userid = $expiredTrainingItem->userid;
				$trainingstatus->expirydate = $expiredtime;
				$trainingstatus->courseid = $expiredTrainingItem->courseid;
				$trainingstatus->timecreated = time();
				$trainingstatus->gradeitemid = $recurringblockconfig->recurringgradeid;
				$trainingstatus->status = 1; // Assume 1 is Expired...

				$existingRecord = $DB->get_record_sql("SELECT * FROM {block_recurring_traininginfo} WHERE userid = ? and courseid = ? AND gradeitemid = ?", array($expiredTrainingItem->userid, $expiredTrainingItem->courseid, $recurringblockconfig->recurringgradeid));

				if ($existingRecord) {
					$trainingstatus->id = $existingRecord->id;
					$DB->update_record('block_recurring_traininginfo', $trainingstatus);
					unset($trainingstatus->id);
					// $DB->insert_record('block_recurring_logs', $trainingstatus);
				} else {
					$DB->insert_record('block_recurring_traininginfo', $trainingstatus);
					// $DB->insert_record('block_recurring_logs', $trainingstatus);
				}
			}

			mtrace('Adding Non-Expired Records');

			$notExpiredTrainingItems = $DB->get_recordset_sql("SELECT GG.*, GI.courseid FROM mdl_grade_grades GG, mdl_grade_items GI WHERE GG.itemid = GI.id AND GG.itemid = $recurringblockconfig->recurringgradeid AND GG.timemodified >= (SELECT UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL $competencylengthdays DAY)))");

			foreach ($notExpiredTrainingItems as $expiredTrainingItem) {

				$expiredtime = $expiredTrainingItem->timemodified + ($competencylengthdays * 86400);

				$trainingstatus = new \stdClass();
				$trainingstatus->userid = $expiredTrainingItem->userid;
				$trainingstatus->expirydate = $expiredtime;
				$trainingstatus->courseid = $expiredTrainingItem->courseid;
				$trainingstatus->timecreated = time();
				$trainingstatus->gradeitemid = $recurringblockconfig->recurringgradeid;
				$trainingstatus->status = 0; // Assume 1 is Expired...

				$existingRecord = $DB->get_record_sql("SELECT * FROM {block_recurring_traininginfo} WHERE userid = ? and courseid = ? and gradeitemid = ?", array($expiredTrainingItem->userid, $expiredTrainingItem->courseid, $recurringblockconfig->recurringgradeid));

				if ($existingRecord) {
					$trainingstatus->id = $existingRecord->id;
					$DB->update_record('block_recurring_traininginfo', $trainingstatus);
					unset($trainingstatus->id);
					// $DB->insert_record('block_recurring_logs', $trainingstatus);
				} else {
					$DB->insert_record('block_recurring_traininginfo', $trainingstatus);
					// $DB->insert_record('block_recurring_logs', $trainingstatus);
				}
			}

			mtrace('Adding Initial Expiry Records');

			$ungradedTrainingItemsQuery = "		SELECT U.id, U.id as 'userid', (SELECT MIN(UE.timecreated) FROM {user_enrolments} UE, {enrol} E WHERE E.id = UE.enrolid AND E.status = 0 AND E.courseid = C.id AND UE.userid = U.id AND E.status = 0 GROUP BY UE.userid) as 'expirydate', C.id as 'courseid', GI.id as 'gradeitemid', '1' as 'status'
			FROM {grade_items} GI, {user} U, {course} C
			WHERE GI.id = ?
			AND GI.courseid = C.id
			AND U.id IN (SELECT UE.userid FROM {user_enrolments} UE, {enrol} E WHERE E.id = UE.enrolid AND E.status = 0 AND E.courseid = C.id AND UE.userid = U.id AND E.status = 0)
			AND U.id NOT IN (SELECT GG.userid FROM {grade_grades} GG WHERE GG.itemid = GI.id AND GG.finalgrade = 100)
			AND U.id NOT IN (SELECT TI.userid FROM mdl_block_recurring_traininginfo TI WHERE TI.courseid = C.id AND TI.gradeitemid = GI.id)";

			$ungradedTrainingItems = $DB->get_records_sql($ungradedTrainingItemsQuery, array($recurringblockconfig->recurringgradeid));

			foreach ($ungradedTrainingItems as $ungradedTrainingItem) {

				$trainingstatus = new \stdClass();
				$trainingstatus->userid = $ungradedTrainingItem->userid;
				$trainingstatus->expirydate = $expiredtime;
				$trainingstatus->courseid = $ungradedTrainingItem->courseid;
				$trainingstatus->timecreated = time();
				$trainingstatus->gradeitemid = $ungradedTrainingItem->gradeitemid;
				$trainingstatus->status = $ungradedTrainingItem->status; // Assume 1 is Expired...

				$DB->insert_record('block_recurring_traininginfo', $trainingstatus);
				// $DB->insert_record('block_recurring_logs', $trainingstatus);

			}

		}

	}                                                                                                                               
} 