<?php

$tasks = array(                                                                                                              
    array(                                                                                                                          
        'classname' => 'block_recurring\task\cron',                                                                            
        'blocking' => 1,                                                                                                            
        'minute' => '0',                                                                                                            
        'hour' => '3',                                                                                                              
        'day' => '*',                                                                                                               
        'dayofweek' => '*',                                                                                                         
        'month' => '*'                                                                                                              
    )
);